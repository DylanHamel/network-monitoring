# Network Monitoring with Prometheus

The goal of this project is to offer the possibility to add personnal data in Prometheus - Grafana graphs.

Some data can not be retrived with SNMP (no OID) or with Telemetry (not ready).

These informations are available in the CLI.



## Introduction

There are two endpoints in this project :

* `/interfaces_metrics`

  Will retrieve `input_bytes`, `input_packets`, `output_bytes` and `output_packets`.

* `routes_counter`

  WIll retrieve the number of routes present in the global default routing instance.

All data are formatted in Prometheus format :

```
output_bytes{hostname="router01" interface="fxp0" } 9755741 
input_bytes{hostname="router01" interface="fxp0" } 5499995 
input_packets{hostname="router01" interface="fxp0" } 65340 
output_packets{hostname="router01" interface="fxp0" } 59386 
```



## Result 

![prometheus.png](images/prometheus.png)

![prometheus2.png](images/prometheus2.png)

![grafana.png](images/grafana.png)

## How to add your own data points

Here a quick example of how implement a new `endpoint`. This example takes the `routes_counter` endpoint.

### 1. Create a new files in `endpoints/` directory

```shell
mkdir -p endpoints/routing/route_counters.py
touch endpoints/routing/__init__.py
```

### 2. Create a new FLASK `endpoint`

```python
@app.route("/routes_counter", methods=['GET'])
def ep_routes_counter():
    result = routes_counter()
    r = _format_to_prometheus(data=result)
    return Response(
        r,
        status=200,
        mimetype="plain/text"
    )

```

* Set a endpoint name => `@app.route("/routes_counter", methods=['GET'])` => `/routes_counter`.
* Renamce the function => `def ep_routes_counter():`
* Use a new function that will be implemented in the next steps => `result = routes_counter()`

### 3. Create the function

In `endpoints/routing/route_counters.py` file add a new function called `routes_counter()`.

This function has to initialize Nornir object and call a function to retrieve network metrics.

```python
def routes_counter() -> dict:
    nr = init_nornir(ansible=True)
    return n_routes_counter(nr=nr)
```

### 4. Crete the function to parallelize jobs and get datas - Part 1

```python
def n_routes_counter(nr: Nornir) -> dict:
    
    devices = nr.filter()
    if len(devices.inventory.hosts) == 0:
        raise Exception(f"No device selected.")

    output = devices.run(
        task=_routes_counter_juniper,
        on_failed=True,
        num_workers=10
    )
    print_result(output)
```

### 5. Create the function with a `task` in parameter

This task will be executed on each devices in your inventory !

```python
def _routes_counter_juniper(task):
    with Device(
        host=task.host.hostname,
        port=task.host.port,
        user=task.host.username,
        password=task.host.password
    ) as m:

        data = m.rpc.get_route_information(terse=True)

    task.host[NB_ROUTES] = count_juniper_routes(
        data=xmltodict.parse(etree.tostring(data))
    )
```

In this example I'm using `PyEZ` library for Juniper. You can use some other libraries like :

* Requests
* ncclient
* etc.

This function is useless if you would like retrieve data from the CLI.

Use directly a netmiko function included in Nornir

#### Example

```python
from nornir.plugins.tasks.networking import netmiko_send_command

output = task.run(
	name=f"show route",
  task=netmiko_send_command,
  command_string="show route"
)
print(output.result)

```

> I could use NAPALM to retrive informations from Juniper devices.
>
> Have a look on Nornir documentation.

### 6. Format and Filtre command/call output

```python
def count_juniper_routes(data: dict) -> int:
    data = json.dumps(data)
    data = json.loads(data)
    count = 0
    
    for route in data.get('route-information').get('route-table')[0].get('rt'):
        print(route.get('rt-destination'))
        count += 1

    return count
```

### 7. Store the result in a new HOST key

```python
task.host[NB_ROUTES]
```

### 8. Format data in the standard

There is a function to format some data into Prometheus format.

The idea is to format data as follow :

```json
{
  'fxp0': {   
    'input_bytes': '37112249',
    'input_packets': '532764',
    'output_bytes': '75245573',
    'output_packets': '524114'
  },
}
```

So you have to write some code to format data in this model :

```python
for device in devices.inventory.hosts:
        result.append(
            {
                "key": NB_ROUTES,
                "args": {
                    "hostname": device
                },
                "value": devices.inventory.hosts[device][NB_ROUTES]
            }
        )
```

### 9. Return the result

```python
return result
```

and that's it



## Generate Prometheus datas

```python
def _format_to_prometheus(data):
    result = ""
    for i in data:
        payload = ""
        for k,v in i.get("args").items():
            payload += f"{k}=\"{v}\" "
        result += f"{i.get('key')}{{{payload}}} {i.get('value')} \n"

    return result
```

With the following data 

```json
{
  'fxp0': {   
    'input_bytes': '37112249',
    'input_packets': '532764',
    'output_bytes': '75245573',
    'output_packets': '524114'
  },
}
```

Generate :

```
output_bytes{hostname="router01" interface="fxp0" } 9755741 
input_bytes{hostname="router01" interface="fxp0" } 5499995 
input_packets{hostname="router01" interface="fxp0" } 65340 
output_packets{hostname="router01" interface="fxp0" } 59386 
```

