#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import click
from nornir.core import Nornir
from nornir.plugins.functions.text import print_result
from flask import Flask, Response, request
from tools.nornit_tools import init_nornir
from endpoints.routing.route_counters import (
    routes_counter
)
from endpoints.interfaces.interface_metrics import (
    interfaces_metrics
)
from constantes.c import (
    NB_ROUTES,
    INT_METRICS
)
app = Flask(__name__)


def _format_to_prometheus(data):
    result = ""
    for i in data:
        payload = ""
        for k,v in i.get("args").items():
            payload += f"{k}=\"{v}\" "
        result += f"{i.get('key')}{{{payload}}} {i.get('value')} \n"

    return result

@app.route("/routes_counter", methods=['GET'])
def ep_routes_counter():
    result = routes_counter()
    r = _format_to_prometheus(data=result)
    return Response(
        r,
        status=200,
        mimetype="plain/text"
    )

@app.route("/interfaces_metrics", methods=['GET'])
def ep_interfaces_metrics():
    result = interfaces_metrics()
    r = _format_to_prometheus(data=result)
    return Response(
        r,
        status=200,
        mimetype="plain/text"
    )

@click.command()
@click.version_option(version="© Dylan Hamel v0.0.1")
@click.option(
    "-t",
    "--test",
    default=False,
    show_default=True,
    help=f"Define path to the production Ansible inventory file",
)
def filter(test):
    if test:
        result = interfaces_metrics()
        #result = routes_counter()
        return False
    else:
        return True

if __name__ == "__main__":
    #filter()
    app.run(host="0.0.0.0", port=55555, debug=True)
