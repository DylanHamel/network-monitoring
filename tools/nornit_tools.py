#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from nornir import InitNornir
from nornir.core import Nornir


def init_nornir(
    log_file="./nornir/nornir.log",
    log_level='debug',
    ansible=False,
) -> Nornir:
    """
    Initialize Nornir object with the following files
    """

    config_file = str()
    if ansible:
        config_file = "./nornir/config_ansible.yml"
    else:
        config_file = "./nornir/config_std.yml"

    nr = InitNornir(
        config_file=config_file,
        logging={"file": log_file, "level": log_level}
    )

    return nr
