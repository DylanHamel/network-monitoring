#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import xmltodict
from lxml import etree
from jnpr.junos import Device
from nornir.core import Nornir
from xml.etree import ElementTree
from constantes.c import INT_METRICS
from tools.nornit_tools import init_nornir
from nornir.plugins.functions.text import print_result
import pprint
PP = pprint.PrettyPrinter(indent=4)


def interfaces_metrics() -> dict:
    nr = init_nornir(ansible=True)
    return n_interfaces_metrics(nr=nr)


def n_interfaces_metrics(nr: Nornir) -> dict:

    devices = nr.filter()
    if len(devices.inventory.hosts) == 0:
        raise Exception(f"No device selected.")

    output = devices.run(
        task=_interfaces_metrics_juniper,
        on_failed=True,
        num_workers=10
    )
    print_result(output)

    result = list()

    for device in devices.inventory.hosts:
        for i,v in devices.inventory.hosts[device][INT_METRICS].items():
            for k,w in v.items():
                result.append(
                    {
                        "key": k,
                        "args": {
                            "hostname": device,
                            "interface": i
                        },
                        "value": w
                    }
                )

    return result


def _interfaces_metrics_juniper(task):
    with Device(
        host=task.host.hostname,
        port=task.host.port,
        user=task.host.username,
        password=task.host.password
    ) as m:

        data = m.rpc.get_interface_information(detail=True)

        task.host[INT_METRICS] = format_interfaces_metrics(
            data=xmltodict.parse(etree.tostring(data))
        )


def format_interfaces_metrics(data: dict) -> int:
    data = json.dumps(data)
    data = json.loads(data)

    result = dict()

    for i in data.get('interface-information').get('physical-interface'):
        if 'traffic-statistics' in i.keys():    
            if (
                'input-bytes' in i.get('traffic-statistics').keys() and
                'output-bytes' in i.get('traffic-statistics').keys()
            ):
                result[i.get('name')] = dict()
                result[i.get('name')]['output_bytes'] = i.get('traffic-statistics') \
                                                         .get('output-bytes')
                result[i.get('name')]['input_bytes'] = i.get('traffic-statistics') \
                                                        .get('input-bytes')
                result[i.get('name')]['input_packets'] = i.get('traffic-statistics') \
                                                          .get('input-packets')
                result[i.get('name')]['output_packets'] = i.get('traffic-statistics') \
                                                           .get('output-packets')

    PP.pprint(result)
    return result
