#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import xmltodict
from lxml import etree
from jnpr.junos import Device
from nornir.core import Nornir
from xml.etree import ElementTree
from constantes.c import NB_ROUTES
from tools.nornit_tools import init_nornir
from nornir.plugins.functions.text import print_result
import pprint
PP = pprint.PrettyPrinter(indent=4)


def routes_counter() -> dict:
    nr = init_nornir(ansible=True)
    return n_routes_counter(nr=nr)


def n_routes_counter(nr: Nornir) -> dict:
    
    devices = nr.filter()
    if len(devices.inventory.hosts) == 0:
        raise Exception(f"No device selected.")

    output = devices.run(
        task=_routes_counter_juniper,
        on_failed=True,
        num_workers=10
    )
    print_result(output)

    result = list()

    for device in devices.inventory.hosts:
        result.append(
            {
                "key": NB_ROUTES,
                "args": {
                    "hostname": device
                },
                "value": devices.inventory.hosts[device][NB_ROUTES]
            }
        )

    return result

def _routes_counter_juniper(task):
    with Device(
        host=task.host.hostname,
        port=task.host.port,
        user=task.host.username,
        password=task.host.password
    ) as m:

        data = m.rpc.get_route_information(terse=True)

    task.host[NB_ROUTES] = count_juniper_routes(
        data=xmltodict.parse(etree.tostring(data))
    )


def count_juniper_routes(data: dict) -> int:
    data = json.dumps(data)
    data = json.loads(data)
    count = 0
    
    for route in data.get('route-information').get('route-table')[0].get('rt'):
        print(route.get('rt-destination'))
        count += 1

    return count
